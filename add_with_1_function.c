//Write the HSPC Program 2007 Problem 2 Egyptian Fractions
#include<stdio.h>
#include<math.h>
#include<conio.h>
  
void FuncEgyptian(int nr, int dr)
{
    // If numerator and denominator is zero
    if (dr == 0 || nr == 0)
        return ;
  
    // numerator / denominator is an integer like: 4/2=2, then simple division
    // makes the fraction in 1/n form
    if (dr%nr == 0)
    {
        printf( "1/%d" , dr/nr);
        return ;
    }
  
    // If denominator divides numerator, then the given number
    // is not fraction
    if (nr%dr == 0)
    {
        printf("%f", nr/dr);
        return;
    }
  
    // numerator > denominator
    if (nr > dr)
    {
        printf("%d", nr/dr);
        printf ( " + ");
        FuncEgyptian(nr%dr, dr);
        return;
    }
    //  dr%nr is non-zero
    // Find ceiling of dr/nr and print it as first fraction
    int n = dr/nr + 1;
    printf( "1/" );
    printf("%d",n);
    printf(" + ");
  
    // Recur for remaining part
    FuncEgyptian((nr*n)-dr, dr*n);
 }
  
// Driver Program
int main()
{
    int nr = 0, dr = 0;
    printf("Enter the Fraction in form of a/b:");
    scanf("%d/%d",&nr,&dr);
    printf("Egyptian Fraction Representation of ");
    printf("%d", nr);
    printf( "/");
    printf("%d", dr);
    printf( " is\n ");
    FuncEgyptian(nr, dr);
    return 0;
}
  
