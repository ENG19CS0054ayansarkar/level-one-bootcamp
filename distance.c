//Write the HSPC Program 2007 Problem 1 Rectangle Area
#include <stdio.h>
#include <math.h>

int main ()
{

  int n=0,i=0;
  printf("Enter the number of Rectangle" );
  scanf("%d",&n);
  for (i =0;i<n;i++){
  float x1=0, x2=0, x3=0, x4=0, y1=0, y2=0, y3=0, y4=0, length, breath, area;
  printf ("enter the X corrdinate");
  scanf ("%f %f %f %f %f %f", &x1, &y1, &x2, &y2, &x3, &y3);

  length=sqrt((x2-x1)(x2-x1)+(y2-y1)(y2-y1));
  breath=sqrt((x3-x2)(x3-x2)+(y3-y2)(y3-y2));
  area=length*breath;

  printf ("area of first rectangle: %f", area);
  }
  return 0;
}
